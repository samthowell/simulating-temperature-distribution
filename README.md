# Thin-Film Temperature Distribution

Simulates the temperature distribution inside a polymer thin film using FEniCS.