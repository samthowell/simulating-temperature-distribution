#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  2 17:33:15 2018

@author: samuel
"""

def convert_geo_to_msh(fname, options):
    """
    Renders a gmsh script into a msh file
    
    Note: works unfortunately not under IPython with python 2.7.13
    
    Input: 
        fname: Filename without extension
        options: Options for gmsh, e.g. "-2" for 2d mesh or "-3" for 3d mesh
    
    Output:
        0 if successful, 1 if it failed
    
    Example:
        >>> convert_geo_to_msh('filename', '-2')
        0
    """
    from subprocess import call, Popen
    
    interpreter = 'gmsh'
    options = str(options)
    fname_in = fname + '.geo'
    
    p = call([interpreter, options, fname_in])
    
    print('Works not under ipython console')
    
    if p == 0:
        print('Sucessfully converted {:s}'.format(fname_in))
    else:
        print('Failed to convert {:s} using gmsh'.format(fname_in))

    return p


def convert_msh_to_xml(fname):
    """
    Convert a mesh file into a xml file using dolfin-convert
    
    Input: 
        fname: Filename without extension
    
    Output:
        0 if successful, 1 if it failed
        
    Example:
        >>> convert_msh_to_xml('filename')
        0
    """
    from subprocess import call, Popen
    
    interpreter = 'dolfin-convert'

    fname_in = fname + '.msh'
    fname_out = fname + '.xml'
    
    p = call([interpreter, fname_in, fname_out])
    
    if p == 0:
        print('Sucessfully converted {:s} to {:s}'.format(fname_in, fname_out))
    else:
        print('Failed to convert {:s} to {:s} using dolfin-convert'.format(fname_in, fname_out))

    return p


#Todo implement mesh_dim, add save_geo_str to line 91 and put a sleep in between
def create_mesh(fname, mesh_params, gmsh_cmd, update=False):
    """Create the mesh from a .geo file into .xml format for fenics
    Update only if .xml file does not exist or update is True
    
    Input:
        fname: filename without extension
        opt_gmsh: options for gmsh 
        update: if true, the mesh is updated even if the files already exist
        
    Output: 
        0
    
    Example:
        >>>create_mesh('filename', '-2')
        0
    """
    from os.path import isfile
    from geohelp import save_geo_str
    from time import sleep
    
    if update == True:
        
        # Save the geo file
        save_geo_str(fname, **mesh_params)
        sleep(2)
        
        # Convert geo -> msh -> xml
        convert_geo_to_msh(fname, gmsh_cmd)
        convert_msh_to_xml(fname)
        sleep(2)
    
    elif isfile(fname+'.xml') == False:
        
        create_mesh(fname, mesh_params, gmsh_cmd, update=True)
        
    
    else:
        
        print('Mesh was not updated.')
    
    return 0 
        

def load_mesh(fname):
    """
    Load a mesh, physical and facet region
    
    Input:
        fname: filename of the mesh without extension
    
    Output:
        mesh:
        cd: information about interior regions
        fd: information about boundaries
    
    Example:
        >>>mesh, cd, fd = load_mesh('filename')
    """
    from fenics import Mesh, MeshFunction
    
    mesh = Mesh(fname+'.xml')
    cd = MeshFunction('size_t', mesh, fname+'_physical_region.xml')
    fd = MeshFunction('size_t', mesh, fname+'_facet_region.xml')
    
    return mesh, cd, fd
    

def pv_csv_import(fname, n_files):
    
    import numpy as np
    
    for i in range(n_files):
        
        data = np.genfromtxt(fname+str(i)+'.csv', skip_header=1, delimiter=',', unpack=True)

        if i == 0:
            container = np.zeros((n_files, data.shape[1]))
            container[i] = data[0]
        
        container[i+1] = data[1]
        
    return container
    

