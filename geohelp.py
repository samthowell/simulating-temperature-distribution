#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Aug  5 21:37:45 2018

@author: samuel
"""

def replace_in_file(string, kwdict):
    """
    Replaces placeholders of the form '{placeholder}' with dict values of the form {'placeholder': value}
    
    Input:
        string containing placeholders of the form {placeholder}
        dict with key, value pairs of the form {'placeholder': value}
    
    Output:
        formatted string with replaced placeholders
        
    Example:
        >>>string = 'hello {people}'
        >>>strformated = replace_in_file(string, {'people': 'world'})
        'hello world'
    """
    import re
    
    for key, value in zip(kwdict.keys(), kwdict.values()):
        string = re.sub('{{{:s}}}'.format(key), str(value), string)    
    return string


def save_geo_str(filename, **kwargs):
    """
    Saves a .geo file from a string formattable string under filename.geo.
    
    Input:
        arguments for string format
        
    Output:
        0
        
    Example:
        
        >>>string = 'h = DefineNumber[ {height}, Name "Parameters/Indent height" ];'
        >>>save_geo_str('default', string, height=1e-6)
        'h = DefineNumber[ {1e-6}, Name "Parameters/Indent height" ];'

    """
    from time import sleep
    print('Use mesh parameters:')
    print(kwargs)
    
    with open(filename+'-tmpl.geo', 'r') as f:
        string = f.read()
    
    with open(filename+'.geo', 'w') as f:
        
        strformat = replace_in_file(string, kwargs)
        f.write(strformat)
    
    print()
    print('Created geo file:')
    print(strformat)
    
    sleep(3)
    
    return 0
