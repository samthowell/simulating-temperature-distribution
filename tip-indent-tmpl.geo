angle = DefineNumber[ {angle}, Name "Parameters/Tip angle" ];
h = DefineNumber[ {height}, Name "Parameters/Indent height" ];
w = DefineNumber[ {width}, Name "Parameters/Substrate width" ];
t = DefineNumber[ {thickness}, Name "Parameters/Polymer thickness" ];
hdmsh = DefineNumber[ {hdmsh}, Name "Parameters/High mesh density" ];
ldmsh = DefineNumber[ {ldmsh}, Name "Parameters/Low mesh density" ];
r = DefineNumber[ Tan( angle/180*Pi )*h, Name "Parameters/Indent radius" ];
d = DefineNumber[ {depth}, Name "Parameters/Substrate depth" ];

Point(1) = {r, 0, 0, hdmsh};
Point(2) = {w, 0, 0, ldmsh};
Point(3) = {w, 0, -t, ldmsh};
Point(4) = {0, 0, -t, ldmsh};
Point(5) = {0, 0, -h, hdmsh};
Point(6) = {w, d, 0, ldmsh};
Point(7) = {w, d, -t, ldmsh};
Point(8) = {0, d, 0, ldmsh};
Point(9) = {0, d, -t, ldmsh};
Point(10) = {0, r, 0, hdmsh};

// Front surface
Line(1) = {1, 5};
Line(2) = {2, 1};
Line(3) = {3, 2};
Line(4) = {4, 3};
Line(5) = {5, 4};
Line Loop(6) = {5, 4, 3, 2, 1};
Plane Surface(22) = {6};

// Tip surface 9
surfVect[] = Extrude {{0, 0, 1}, {0, 0, 0}, Pi/2} {
	Line{1};
}; // Axis direction, coordinate of an axis point, angle in radians

Characteristic Length {1, 5, 10, 23, 24} = hdmsh;

// Left surface
// Line 24 top from extrusion
// Line 23 corresponding to 1 from extrusion
Line(9) = {10, 8};
Line(10) = {8, 9};
Line(11) = {9, 4};
Line Loop(12) = {-23, 9, 10, 11, -5};
Plane Surface(23) = {12};

// Back surface
Line(13) = {8, 6};
Line(14) = {6, 7};
Line(15) = {7, 9};
Line Loop(16) = {13, 14, 15, -10};
Plane Surface(24) = {16};

// Right surface
Line(17) = {6, 2};
Line(18) = {3, 7};
Line Loop(19) = {17, -3, 18, -14};
Plane Surface(26) = {19};

// Top surface
Line Loop(20) = {-2, -17, -13, -9, -24};
Plane Surface(27) = {20};

// Bottom surface
Line Loop(21) = {-4, -18, -15, -11};
Plane Surface(28) = {21};

// Volume
Surface Loop(29) = {27, 22, 23, 25, 24, 26, 28};
Volume(30) = {29};


// Physical Groups
Physical Surface("heater") = {25};
Physical Surface("mirror") = {23, 6};
Physical Surface("heat sink") = {28, 26, 24};
Physical Surface("air") = {27};
Physical Volume("body") = {30};
